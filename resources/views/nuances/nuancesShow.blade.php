@extends('template')

@section('title')
  Show nuance
@endsection

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header"> Fiche nuance
          </div>
          @if(Auth::check() and Auth::user()->admin)
            {{-- <p>Id :             {{ $nuance->id }}</p> --}}
          @endif
          <p>Nom :              {{ $nuance->nuanceName}}</p>
          <p>Famille :          {{ $nuance->famille}}</p>
          <p>Densité :          {{ $nuance->densite}}</p>
          <table>
            @foreach ($nuance->designations as $designation)
              <tr>
                <td class="col-sm-4">{{ $designation->designationNorme }}</td>
                <td class="col-sm-4"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></td>
                <td class="col-sm-4">{{ $designation->designationName }}</td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
  <a href="javascript:history.back()" class="btn bg-lightGrey"> <span class="fa fa-arrow-circle-left"> </span> Retour </a>
@endsection
