@extends('template')

@section('title')
  Users index
@endsection

@section('content')
  <div class="col-sm-8">
    @if(session()->has('ok'))
      <div class="alert alert-success alert-dismissible">{!! session('ok') !!}</div>
    @endif
    <div class="card">
      <div class="card-header bg-lightGrey">
        <h3>Liste des utilisateurs</h3>
      </div>
      <table class="table table-dark table-striped">
        <thead>
          <tr>
            <th class="col-sm-2">#</th>
            <th class="col-sm-6">email</th>
            <th class="col-sm-2"></th>
            <th class="col-sm-2"></th>
            <th class="col-sm-2"></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($users as $user)
            <tr>
              <td>{!! $user->id !!}</td>
              <td class="text-primary"><strong>{!! $user->email !!}</strong></td>
              <td>{!! link_to_route('user.show', 'Voir', [$user->id], ['class' => 'btn bg-green btn-block']) !!}</td>
              <td>{!! link_to_route('user.edit', 'Modifier', [$user->id], ['class' => 'btn bg-yellow btn-block']) !!}</td>
              <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['user.destroy', $user->id]]) !!}
                {!! Form::submit('Supprimer', ['class' => 'btn bg-red btn-block', 'onclick' => 'return confirm(\'Vraiment supprimer cet utilisateur ?\')']) !!}
                {!! Form::close() !!}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    {!! link_to_route('user.create', 'Ajouter un utilisateur', [], ['class' => 'btn bg-lightBlue float-right']) !!}
    {!! $links !!}
  </div>
@endsection
