<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ChuteRepository;

use App\Models\Chute;

class ChuteController extends Controller
{

  public function __construct(ChuteRepository $chuteRepository)
  {
    $this->chuteRepository = $chuteRepository;
  }

  public function index()
  {
    $chutes = $this->chuteRepository->getPaginate(25);
    $links = $chutes->render();
    return view('chutes.chutes', compact('chutes', 'links'));
  }

  public function create()
  {
    return view('chutes.chutesAdd');
  }

  public function store(Request $request)
  {

  }

  public function show($id)
  {

  }

  public function edit($id)
  {

  }

  public function update($id)
  {

  }

  public function destroy($id)
  {

  }
}
?>
