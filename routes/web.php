<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::auth();
Auth::routes();

Route::any('/search', 'NuanceController@search');

Route::resource('user', 'UserController');

Route::resource('chute', 'ChuteController');
Route::resource('nuance', 'NuanceController');
Route::get('nuance/designation/{designation}', 'NuanceController@indexDesignation');
// Route::resource('designation', 'DesignationController');
Route::resource('commande', 'CommandeController');
