@extends('templateUserForm')

@section('title')
  Create user
@endsection

@section('card-header')
  <div class="card-header bg-lightGrey">{{__('Create') }}
  </div>
@endsection

@section('form-open')
  <form method="POST" action="{{ route('user.store') }}">
@endsection

@section('submit')
  <button type="submit" class="btn bg-lightGreen float-right"> {{ __('Create') }} </button>
@endsection
