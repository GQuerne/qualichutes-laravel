<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
  // column allowed to be affected by create
  protected $fillable = [
    'nuance_id', 'DesignationNorme', 'designationName'
  ];
  // une designations appartient à une nuance
  public function nuance()
  {
    return $this->belongsTo('App\Models\Nuance');
  }

}
