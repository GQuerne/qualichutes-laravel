@extends('template')

@section('content')
	<div class="container">
		<div class="card col-md-10">
			<div class="card-header bg-lightGrey">
				Ajouter une nuance
			</div>
			<div class="card-body">
				{!! Form::open(array('route' => 'nuance.store', 'method' => 'POST')) !!}
				{{-- famille --}}
				<div class="form-group row">
					{!! Form::label('famille', 'Famille:', ['class' => 'col-md-3 col-form-label text-md-right']) !!}
					<div class="col-md-9">
						{!! Form::radio('famille', 'acConstrAllie') !!}	Aciers de Construction et Alliés	<br>
						{!! Form::radio('famille', 'acInox') !!}				Aciers Inoxydables								<br>
						{!! Form::radio('famille', 'acoutils') !!}			Aciers Outils											<br>
						{!! Form::radio('famille', 'plastTech') !!}			Plastiques techniques							<br>
						{!! Form::radio('famille', 'alu') !!}						Aluminiums												<br>
						{!! Form::radio('famille', 'cuivre') !!}				Cuivreux													<br>
						{!! Form::radio('famille', 'bronze') !!}				Bronzes														<br>
						{!! Form::radio('famille', 'acEtire') !!}				Aciers Outils											<br>
						{!! Form::radio('famille', 'tubeMeca') !!}			Tubes Mécaniques									<br>
						{!! Form::radio('famille', 'toleBleue') !!}			Tôles Bleues											<br>
						{!! Form::radio('famille', 'tigeChrome') !!}		Tiges Chromées										<br>
						{!! Form::radio('famille', 'fonte') !!}					Fontes														<br>
						{!! Form::radio('famille', 'acRessort') !!}			Aciers à Ressort									<br>
					</div>
				</div>
				{{-- name --}}
				<div class="form-group row">
					{!! Form::label('nuanceName', 'Name:', ['class' => 'col-md-3 col-form-label text-md-right']) !!}
					<div class="col-md-9">
						{!! Form::text('nuanceName', null, ['placeholder' => 'nom']) !!}
					</div>
				</div>
				{{-- désignations --}}
				<div class="form-group row">
					{!! Form::label('designations', 'Désignations:', ['class' => 'col-md-3 col-form-label text-md-right']) !!}
					<ul id="designation_table" class="col-md-9" style="list-style-type:none;">
						<li id="row1">
								{!! Form::text('designationNorme[]', null, ['placeholder' => 'norme', 'class' => 'col-md-3']) !!}
								{!! Form::text('designationName[]', null, ['placeholder' => 'name', 'class' => 'col-md-3']) !!}
						</li>
						<input type="button" onclick="add_row();" value="Add Row" class="btn bg-lightBlue float-left">
					</ul>

				</div>
				{{-- densite --}}
				<div class="form-group row">
					{!! Form::label('densite', 'Densite:', ['class' => 'col-md-3 col-form-label text-md-right']) !!}
					<div class="col-md-9">
						{!! Form::text('densite') !!}
					</div>
				</div>
				{!! Form::submit('Ajouter', ['class' => 'btn bg-lightGreen float-right']) !!}
				{!! Form::close() !!}
				<a href="javascript:history.back()" class="btn bg-lightGrey float-left"> <span class="fa fa-arrow-circle-left"> </span> Retour </a>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
function add_row()
{
	$rowno=$("#designation_table li").length;
	$rowno=$rowno+1;
	$("#designation_table li:last").after("<li id='row"+$rowno+"'><input type='text' name='designationNorme[]' placeholder='norme' class='col-md-3'> <input type='text' name='designationName[]' placeholder='name' class='col-md-3'> <input type='button' value='Delete' class='bg-lightGrey col-md-3' onclick=delete_row('row"+$rowno+"')></li>");
}
function delete_row(rowno)
{
	$('#'+rowno).remove();
}
</script>

@endsection
