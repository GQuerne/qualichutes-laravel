<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignationsTable extends Migration
{
  public function up()
  {
    Schema::create('designations', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();
      $table->integer('nuance_id')->unsigned(); //foreignKey
      $table->foreign('nuance_id')->references('id')->on('nuances')->onDelete('restrict')->onUpdate('restrict');
      $table->string('designationNorme')->nullable();
      $table->string('designationName');
    });
  }

  public function down()
  {
    Schema::dropIfExists('designations');
  }
}
