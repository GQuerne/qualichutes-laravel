<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChutesTable extends Migration
{
  public function up()
  {
    Schema::create('chutes', function (Blueprint $table) {
      $table->increments('id');
      $table->timestamps();
      $table->integer('nuance_id')->unsigned(); //foreignKey
      $table->foreign('nuance_id')->references('id')->on('nuances')->onDelete('restrict')->onUpdate('restrict');
      $table->string('CCPU')->nullable();
      $table->enum('format', array('indifférent', 'rond', 'carre', '6Pans', 'tubes'))->default('indifférent');
      $table->string('diam')->nullable();
      $table->string('epaisseur')->nullable();
      $table->string('cote')->nullable();
      $table->string('longueurCoupe')->nullable();
      $table->string('poidsTheorique')->nullable(); // fill by controller dimensions*nuance->densite
      $table->float('poidsReel')->nullable();
      $table->float('prixHorsTaxe')->nullable();
    });
  }

  public function down()
  {
    Schema::dropIfExists('chutes');
  }
}
