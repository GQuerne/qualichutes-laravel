@extends('template')

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
				<div class="card">
					<div class="card-header bg-lightGrey">
						Ajouter une chute
					</div>
					<div class="card-body">
						{!! Form::open(array('route' => 'chute.store', 'method' => 'POST')) !!}
						{{-- format --}}
						<div class="form-group row">
							{!! Form::label('format', 'Format:', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
							{!! Form::text('format', null, ['class' => 'col-md-8']) !!}
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>



	{!! Form::open(array('route' => 'chute.store', 'method' => 'POST')) !!}
		<ul>
			<li>
				{!! Form::label('nuance_id', 'Nuance_id:') !!}
				{!! Form::text('nuance_id') !!}
			</li>
			<li>
				{!! Form::label('CCPU', 'CCPU:') !!}
				{!! Form::text('CCPU') !!}
			</li>
			<li>
				{!! Form::label('format', 'Format:') !!}
				{!! Form::text('format') !!}
			</li>
			<li>
				{!! Form::label('dimensions', 'Dimensions:') !!}
				{!! Form::text('dimensions') !!}
			</li>
			<li>
				{!! Form::label('poidsTheorique', 'PoidsTheorique:') !!}
				{!! Form::text('poidsTheorique') !!}
			</li>
			<li>
				{!! Form::label('poidsReel', 'PoidsReel:') !!}
				{!! Form::text('poidsReel') !!}
			</li>
			<li>
				{!! Form::label('prixHorsTaxe', 'PrixHorsTaxe:') !!}
				{!! Form::text('prixHorsTaxe') !!}
			</li>
			<li>
				{!! Form::submit() !!}
			</li>
		</ul>
	{!! Form::close() !!}
@endsection
