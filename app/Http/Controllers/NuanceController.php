<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\NuanceCreateRequest;
use App\Http\Requests\NuanceUpdateRequest;

use App\Repositories\NuanceRepository;

use App\Models\Nuance;
use App\Models\Designation;

class NuanceController extends Controller
{
  protected $nuanceRepository;
  protected $nbrPerPage = 12;

  public function __construct(NuanceRepository $nuanceRepository)
  {
    // seul un administrateur peut créer, éditer ou détruire une nuance
    $this->middleware('admin', ['only' => 'create', 'edit', 'destroy']);
    $this->nuanceRepository = $nuanceRepository;
  }

  // catalogue de nuance
  public function index()
  {
    $nuances = $this->nuanceRepository->getWithDesignationAndPaginate($this->nbrPerPage);
    // translate nuance->famille from DB to user eyes
    foreach ($nuances as $nuance) {
      switch($nuance->famille) {
        case 'acConstrAllie' :
        $family = 'Aciers de Constructions et Alliés';
        break;
        case 'acInox' :
        $family = 'Aciers Inoxydables';
        break;
        case 'acOutils' :
        $family = 'Aciers Outils';
        break;
        case 'plastTech' :
        $family = 'Plastiques Techniques';
        break;
        case 'alu' :
        $family = 'Aluminiums';
        break;
        case 'cuivre'  :
        $family = 'Cuivreux';
        break;
        case 'bronze'  :
        $family = 'Bronze';
        break;
        case 'acEtire'  :
        $family = 'Aciers Étirés';
        break;
        case 'tubeMeca'  :
        $family = 'Tubes Mécaniques';
        break;
        case 'tigeChrome'  :
        $family = 'Tiges Chromées';
        break;
        case 'fonte' :
        $family = 'Fontes';
        break;
        case 'toleBleue' :
        $family = 'Tôles Bleues';
        break;
        case 'megaStubs' :
        $family = 'MégaStubs';
        break;
        case 'acRessort'  :
        $family = 'Aciers à Ressort';
        break;
      }
      $nuance->famille = $family;
    }
    $links = $nuances->render();
    return view('nuances.nuances', compact('nuances', 'links'));
  }

  // load create form
  public function create()
  {
    return view('nuances.nuancesAdd');
  }

  // insert new nuance, from create form with one designationNorme->designationName
  public function store(NuanceCreateRequest $request)
  {
    // $data = Nuance::create([
    //   'famille' => $request->famille, 'nuanceName' => $request->nuanceName, 'densite' => $request->densite,
    // ]);
    // Designation::insert([
    //   'nuance_id' => $data->id, 'designationNorme' => $request->designationNorme, 'designationName' => $request->designationName,
    // ]);
    print_r($request->all());

    $famille = $request->famille;
    $nuanceName = $request->nuanceName;
    $densite = $request->densite;
    $data = Nuance::create([
      'famille' => $famille, 'nuanceName' => $nuanceName, 'densite' => $densite,
    ]);

    $nuance_id = $data->id;
    $designationNorme = $request['designationNorme'];
    $designationName = $request['designationName'];
    for($i=0;$i<count($designationName);$i++)
    {
      Designation::create([
        'nuance_id' => $nuance_id, 'designationNorme' => $designationNorme[$i], 'designationName' => $designationName[$i]]);
      // "insert into designations values('$name[$i]','$age[$i]','$job[$i]')");
    }

    return redirect('nuance')->withOk("La nuance" . $request->nuanceName . "a été créée");
  }

  // load a specific page for a nuance
  public function show($id)
  {
    $nuance = $this->nuanceRepository->getByIdWithDesignation($id);
    return view('nuances.nuancesShow', compact('nuance'));
  }

  // load edit form
  public function edit($id)
  {
    $nuance = $this->nuanceRepository->getById($id);
    return view('nuances.nuancesEdit', compact('nuance'));
  }

  // update an existing nuance, from edit form
  public function update(NuanceUpdateRequest $request, $id)
  {
    /*//////////////////////////////////
    // supposed to edit designations
    if(isset($inputs['designations']))
    {
    $designationRepository->store($nuance, $inputs['designations']);
  }
  $this->nuanceRepository->update($id, $request->all());
  ///////////////////////////////////////////////////////*/
  $nuance = $this->nuanceRepository->update($id, [
    'famille' => $request->famille, 'nuanceName' => $request->nuanceName, 'densite' => $request->densite,
  ]);
  Designation::updateOrCreate([
    'nuance_id' => $id, 'designationNorme' => $request->designationNorme, 'designationName' => $request->designationName,
  ]);
  return redirect('nuance');
}

// delete a nuance
public function destroy($id)
{
  $nuance = $this->nuanceRepository->getById($id);
  $nuance->designations()->delete();
  $this->nuanceRepository->destroy($id);
  return back();
}

// prepare search by designation
public function indexDesignation($designation)
{
  $nuances = $this->nuanceRepository->getWithDesignationForSearch($designation, $this->nbrPerPage);
  $links = $nuances->render();
  return view('nuances.nuances', compact('nuances', 'links'))->with('info', 'Résultats pour la recherche :' .$designation);
}

public function search(Request $request)
{
  $q = $request->input('q');
  $nuance = $this->nuanceRepository->getWithDesignationForSearch($q, $this->nbrPerPage);
  if(count($nuance) > 0)
  {
    return view('nuances.nuancesSearch')->withDetails($nuance)->withQuery($q);
  } else {
    return view('nuances.nuancesSearch')->withMessage('No nuances found. Try to search again!');
  }
}

}

?>
