<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chute extends Model
{
  // column allowed to be affected by create
  protected $fillable = [
    'CCPU', 'format', 'dimensions', 'poidsTheorique', 'poids reel', 'prixHorsTaxe'
  ];
  // une chute appartient à une nuance
  public function nuance()
  {
    return $this->belongsTo('App\Models\Nuance');
  }

}
