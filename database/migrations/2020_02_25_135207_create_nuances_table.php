<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNuancesTable extends Migration
{
  public function up()
  {
    Schema::create('nuances', function (Blueprint $table) {
      $table->increments('id');
			$table->timestamps();
			$table->enum('famille', array('acConstrAllie', 'acInox', 'acOutils', 'plastTech', 'alu', 'cuivre', 'bronze' , 'acEtire', 'tubeMeca', 'tigeChrome', 'fonte', 'toleBleue', 'megaStub', 'acRessort'))->nullable();
      $table->string('nuanceName')->unique();
			$table->string('densite');
    });
  }

  public function down()
  {
    Schema::dropIfExists('nuances');
  }
}
