<?php

use Illuminate\Database\Seeder;

// $table->bigIncrements('id');
// $table->string('email')->unique();
// $table->string('password');
// $table->enum('gender', ['Mr', 'Mme', 'NC'])->nullable();
// $table->string('lastName')->nullable();
// $table->string('firstName')->nullable();
// // $table->timestamp('email_verified_at')->nullable();
// $table->string('company')->nullable();
// $table->string('siret')->unique()->nullable();
// $table->string('addressLine1');
// $table->string('addressLine2')->nullable();
// $table->string('postCode');
// $table->string('city');
// $table->string('country');
// $table->string('tel');
// $table->string('fax')->nullable();
// $table->enum('pay', ['LCR', 'virement', 'paypal', 'chèque'])->nullable();
// $table->enum('fidel', ['newcomer', 'gold', 'platinium'])->nullable();
// $table->boolean('admin')->default(false);
// $table->rememberToken();
// $table->timestamps();

class UsersTableSeeder extends Seeder
{
  public function run()
  {
    DB::table('users')->delete();

    for ($i=1 ; $i <= 10 ; $i++ )
    {
      DB::table('users')->insert([
        'email' => 'email' . $i . '@test.fr',
        'password' => bcrypt('password' .$i),
        'gender' => 'Mr',
        'lastName' => 'Nom' . $i,
        'firstName' => 'Prénom' . $i,
        'company' => 'Société' . $i,
        'siret' => 'Société' . $i . 'S.' . rand(),
        'addressLine1' => 'street' . $i,
        'addressLine2' => 'streetSuite' . $i,
        'postCode' => rand(00001, 99999),
        'city'  => 'city' . $i,
        'country' => 'country' . $i,
        'tel' => '06 00 00 00 00',
        'fax' => '01 00 00 00 00',
        'pay' => 'paypal',
        'fidel' => 'newcomer',
        'admin' => rand(0 , 1)
      ]);
    }
  }
}

?>
