<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

// $table->increments('id');
// $table->timestamps();
// $table->enum('famille', array(''));
// $table->string('densite');

class DesignationsTableSeeder extends Seeder
{
  private function randDate()
  {
    return Carbon::createFromDate(null, rand(1, 12), rand(1, 28));
  }

  public function run()
  {
    DB::table('designations')->delete();

    for ($i=1 ; $i <= 124 ; $i++ ) {
      for ($j=1; $j <=8 ; $j++) {
        $date = $this->randDate();
        switch ($j) {
          case '1':
          $norme = 'AFNOR';
          break;
          case '2':
          $norme = 'EuroNorme';
          break;
          case '3':
          $norme = 'NF';
          break;
          case '4':
          $norme = 'AISI';
          break;
          case '5':
          $norme = 'DIN';
          break;
          case '6':
          $norme = 'WerKstoff';
          break;
          case '7':
          $norme = 'BS';
          break;
          case '8':
          $norme = 'UNS';
          break;
        }
        DB::table('designations')->insert([
          'nuance_id' => $i,
          'designationNorme' => $norme,
          'designationName' => 'default',
          'updated_at' => $date
        ]);
      }
    }
  }
}

?>
