@extends('template')

@section('title')
  Show user
@endsection

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header"> Fiche utilisateur
          </div>
          <p>Email :            {{ $user->email }}</p>
          <p>Gender :           {{ $user->gender }}</p>
          <p>Last Name :        {{ $user->lastName }}</p>
          <p>First Name :       {{ $user->firstName }}</p>
          <p>Company :          {{ $user->company }}</p>
          <p>SIRET :            {{ $user->siret }}</p>
          <p>Address :          {{ $user->addressLine1 }}</p>
          <p>Addres suite :     {{ $user->addressLine2 }}</p>
          <p>Postal Code :      {{ $user->postCode }}</p>
          <p>City :             {{ $user->city }}</p>
          <p>Country :          {{ $user->country }}</p>
          <p>Phone :            {{ $user->tel }}</p>
          <p>Fax :              {{ $user->fax }}</p>
          <p>Payment mode :     {{ $user->pay }}</p>
          <p>Fidelity status :  {{ $user->fidel }}</p>
          @if($user->admin == 1)
            Administrateur
          @endif
        </div>
      </div>
    </div>
  </div>
  <a href="javascript:history.back()" class="btn btn-primary float-left"> <span class="fa fa-arrow-circle-left"> </span> Retour </a>
@endsection
