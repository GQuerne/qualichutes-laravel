@extends('template')

@section('title')
  Edit user
@endsection

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header"> Edit user
          </div>
          {{-- open form --}}
          {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put']) !!}
          {{-- email --}}
          <div class="form-group row">
            {!! Form::label('email', 'e-mail address', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => $user->email]) !!}
              {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- gender --}}
          <div class="form-group row">
            {!! Form::label('gender', 'Gender', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::radio('gender', 'Mr') !!} Mr
              {!! Form::radio('gender', 'Mme') !!} Mme
              {!! Form::radio('gender', 'NC') !!} NC
            </div>
          </div>
          {{-- lastName --}}
          <div class="form-group row">
            {!! Form::label('lastName', 'Nom', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('lastName', null, ['class' => 'form-control', 'placeholder' => $user->lastName]) !!}
              {!! $errors->first('lastName', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- firstName --}}
          <div class="form-group row">
            {!! Form::label('firstName', 'Prénom', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('firstName', null, ['class' => 'form-control', 'placeholder' => $user->firstName]) !!}
              {!! $errors->first('firstName', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- company --}}
          <div class="form-group row">
            {!! Form::label('company', 'Société', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('company', null, ['class' => 'form-control', 'placeholder' => $user->company]) !!}
              {!! $errors->first('company', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- SIRET --}}
          <div class="form-group row">
            {!! Form::label('siret', 'SIRET', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('siret', null, ['class' => 'form-control', 'placeholder' => $user->siret]) !!}
              {!! $errors->first('siret', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- addressLine1 --}}
          <div class="form-group row">
            {!! Form::label('addressLine1', 'Address', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('addressLine1', null, ['class' => 'form-control', 'placeholder' => $user->addressLine1]) !!}
              {!! $errors->first('addressLine1', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- addressLine2 --}}
          <div class="form-group row">
            {!! Form::label('addressLine2', 'Address suite', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('addressLine2', null, ['class' => 'form-control', 'placeholder' => $user->addressLine2]) !!}
              {!! $errors->first('addressLine2', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- postCode --}}
          <div class="form-group row">
            {!! Form::label('postCode', 'Postal Code', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('postCode', null, ['class' => 'form-control', 'placeholder' => $user->postCode]) !!}
              {!! $errors->first('postCode', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- City --}}
          <div class="form-group row">
            {!! Form::label('city', 'City', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => $user->city]) !!}
              {!! $errors->first('city', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- Country --}}
          <div class="form-group row">
            {!! Form::label('country', 'Country', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => $user->country]) !!}
              {!! $errors->first('country', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- phone --}}
          <div class="form-group row">
            {!! Form::label('tel', 'Téléphone', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('tel', null, ['class' => 'form-control', 'placeholder' => $user->tel]) !!}
              {!! $errors->first('tel', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- fax --}}
          <div class="form-group row">
            {!! Form::label('fax', 'Fax', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::text('fax', null, ['class' => 'form-control', 'placeholder' => $user->fax]) !!}
              {!! $errors->first('fax', '<small class="help-block">:message</small>') !!}
            </div>
          </div>
          {{-- pay --}}
          <div class="form-group row">
            {!! Form::label('pay', 'Payement mode', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
            <div class="col-md-6">
              {!! Form::radio('pay', 'LCR') !!}       LCR
              {!! Form::radio('pay', 'virement') !!}  virement
              {!! Form::radio('pay', 'paypal') !!}    paypal
              {!! Form::radio('pay', 'chèque') !!}    chèque
            </div>
          </div>
          {{-- fidelity --}}
            <div class="form-group row">
              {!! Form::label('fidel', 'fidelity status', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
              <div class="col-md-6">
                {!! Form::radio('fidel', 'newcomer') !!}  Newcomer
                {!! Form::radio('fidel', 'gold') !!}      Gold
                {!! Form::radio('fidel', 'platinium') !!} Platinium
              </div>
            </div>
          {{-- admin --}}
          <div class="form-group">
            <div class="checkbox">
              <label>
                {!! Form::checkbox('admin', 1, null) !!}Administrateur
              </label>
            </div>
          </div>
          {!! Form::submit('Envoyer', ['class' => 'btn btn-primary float-right']) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    <a href="javascript:history.back()" class="btn bg-lightGrey"> <span class="fa fa-arrow-circle-left"> </span> Retour </a>
  </div>


@endsection
