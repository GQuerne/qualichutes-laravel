<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nuance extends Model
{

    protected $fillable = [
        'famille', 'nuanceName', 'densite', 'designationNorme', 'designationName'
    ];

    public function designations()
    {
      return $this->hasMany('App\Models\Designation');
    }

    public function chutes()
    {
      return $this->hasMany('App\Models\Chute');
    }

}
