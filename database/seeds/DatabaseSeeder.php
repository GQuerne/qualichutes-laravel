<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);

        $this->call(NuancesTableSeeder::class);
        $this->call(DesignationsTableSeeder::class);
        $this->call(ChutesTableSeeder::class);

        $this->call(CartsTableSeeder::class);
    }
}
