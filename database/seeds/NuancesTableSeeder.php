<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

// $table->increments('id');
// $table->timestamps();
// $table->enum('famille', array('acConstrAllie', 'acInox', 'acOutils', 'plastTech', 'alu', 'cuivre', 'bronze' , 'acEtire', 'tubeMeca', 'tigeChrome', 'fonte', 'toleBleue', 'megaStub', 'acRessort'))->nullable();
// $table->string('nuanceName')->unique();
// $table->float('densite');

class NuancesTableSeeder extends Seeder
{
  private function randDate()
  {
    return Carbon::createFromDate(null, rand(1, 12), rand(1, 28));
  }

  public function run()
  {
    DB::table('nuances')->delete();
    // aciers de constructions
    for ($i=1 ; $i <=28  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'acConstrAllie',
        'nuanceName' => 'acConstrAllie' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // aciers inoxydables
    for ($i=1 ; $i <=21  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'acInox',
        'nuanceName' => 'acInox' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // aciers outils
    for ($i=1 ; $i <=16  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'acOutils',
        'nuanceName' => 'acOutils' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // plastiques techniques
    for ($i=1 ; $i <=14  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'plastTech',
        'nuanceName' => 'plastTech' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // aluminium
    for ($i=1 ; $i <=11  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'alu',
        'nuanceName' => 'alu' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // cuivres
    for ($i=1 ; $i <=10  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'cuivre',
        'nuanceName' => 'cuivre' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // bronze
    for ($i=1 ; $i <=8  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'bronze',
        'nuanceName' => 'bronze' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // aciers etires
    for ($i=1 ; $i <=6  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'acEtire',
        'nuanceName' => 'acEtire' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // tubes mécaniques
    for ($i=1 ; $i <=3  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'tubeMeca',
        'nuanceName' => 'tubeMeca' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // tiges chromées
    for ($i=1 ; $i <=2  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'tigechrome',
        'nuanceName' => 'tigeChrome' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // fontes
    for ($i=1 ; $i <=2  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'fonte',
        'nuanceName' => 'fonte' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // toles bleues
    for ($i=1 ; $i <=1  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'toleBleue',
        'nuanceName' => 'toleBleue' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // megasstubs
    for ($i=1 ; $i <=1  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'megaStub',
        'nuanceName' => 'megaStub' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
    // acier à ressort
    for ($i=1 ; $i <=1  ; $i++ ) {
      $date = $this->randDate();
      DB::table('nuances')->insert([
        'famille' => 'acRessort',
        'nuanceName' => 'acRessort' . $i,
        'densite' => $i . ',00',
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
  }
}

?>
