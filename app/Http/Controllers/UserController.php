<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;

use App\Repositories\UserRepository;

class UserController extends Controller
{
  protected $userRepository;
  protected $nbrPerPage = 5;

  public function __construct(UserRepository $userRepository)
  {
    $this->userRepository = $userRepository;
  }
  // what you see on qualichutes/user, index of all user, admin interface
  public function index()
  {
    $users = $this->userRepository->getPaginate($this->nbrPerPage);
    $links = $users->render();

    return view('users.index', compact('users', 'links'));
  }
  // load create form, used in register
  public function create()
  {
    return view('users.create');
  }
  // insert new user, from create form
  public function store(UserCreateRequest $request)
  {
    $this->setAdmin($request);

    $user = $this->userRepository->store($request->all());
    return redirect('user')->withOk("L'utilisateur " . $request->email . " a été créé.");
  }
  // load a specific page for an user
  public function show($id)
  {
    $user = $this->userRepository->getById($id);

    return view('users.show',  compact('user'));
  }
  // load edit form
  public function edit($id)
  {
    $user = $this->userRepository->getById($id);

    return view('users.edit',  compact('user'));
  }
  // update an existing user, from edit form
  public function update(UserUpdateRequest $request, $id)
  {
    $this->setAdmin($request);

    $this->userRepository->update($id, $request->all());

    return redirect('user')->withOk("L'utilisateur a été modifié.");
  }
  // delete a nuance
  public function destroy($id)
  {
    $this->userRepository->destroy($id);

    return back();
  }
  // by default admin set to FALSE
  private function setAdmin($request)
  {
    if(!$request->has('admin'))
    {
      $request->merge(['admin' => 0]);
    }
  }
}
