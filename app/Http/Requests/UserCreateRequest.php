<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{

  public function authorize()
  {
    return true;
  }
  // validator
  public function rules()
  {
    return [
      'email' => 'required|confirmed|email|max:50|unique:users',
      'password' => 'required|confirmed|min:6',
      'lastName' => 'required|max:25',
      'firstName' => 'required|max:25',
      'siret' => 'required_with:company|max:25|unique:users',
      'addressLine1' => 'required|max:255',
      'postCode' => 'required|max:25',
      'city' => 'required|max:25',
      'country' => 'required|max:25',
      'tel' => 'required',
    ];
  }
}
