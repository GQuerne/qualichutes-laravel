@extends('template')

@section('title')
  Nuances
@endsection

@section('content')
  @if(Auth::check() and Auth::user()->admin)
    {!! link_to_route('nuance.create', 'Ajouter une nuance', [], ['class' => 'btn bg-lightGreen float-right']) !!}
  @endauth
  @if(isset($info))
    <div class="row alert alert-info"> {{ $info }} </div>
  @endif
  <div class="mt-3 ml-5">
    {!! $links !!}
  </div>

  <div class="row">
    @foreach ($nuances as $nuance)
      <div class="card col-sm-3">
        <div class="card-header bg-lightGrey">
          <a href="nuance/{{$nuance->id}}"> {{ $nuance->nuanceName }} </a>
          <span class="float-right"> {{$nuance->famille}} </span>
        </div>
        <div class="card-body">
          <table>
            @foreach ($nuance->designations as $designation)
              <tr>
                <td class="col-sm-4">{{ $designation->designationNorme }}</td>
                <td class="col-sm-4"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></td>
                <td class="col-sm-4">{{ $designation->designationName }}</td>
              </tr>
            @endforeach
          </table>
        </div>
        @if(Auth::check() and Auth::user()->admin)
          <div class="card-footer">
            <a href="nuance/{{ $nuance->id }}/edit" class="float-left"> Éditer la nuance </a>
            {!! Form::open(['method' => 'DELETE', 'route' => ['nuance.destroy', $nuance->id]]) !!}
            {!! Form::submit('Supprimer cette nuance', ['class' => 'btn btn-danger float-right', 'onclick' => 'return confirm(\'Voulez-vous vraiment supprimer cette nuance ?\')']) !!}
            {!! Form::close() !!}
          </div>
        @endif
      </div>
    @endforeach
  </div>
  {!! $links !!}
@endsection
