<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DesignationRequest extends FormRequest
{

  public function authorize()
  {
    return true;
  }

  // validator
  public function rules()
  {
    return [
      'nuance_id' => 'required',
      'designationName' => 'required'
    ];
  }
}
