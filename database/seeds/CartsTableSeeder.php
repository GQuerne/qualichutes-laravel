<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

// $table->bigIncrements('id');
// $table->timestamps();
// $table->integer('user_id')->unsigned(); //foreignKey
// $table->foreign('user_id')->references('id')->on('user')->onDelete('restrict')->onUpdate('restrict');
// $table->integer('chute_id')->unsigned(); //foreignKey
// $table->foreign('chute_id')->references('id')->on('chute')->onDelete('restrict')->onUpdate('restrict');

class CartsTableSeeder extends Seeder
{
  private function randDate()
  {
    return Carbon::createFromDate(null, rand(1, 12), rand(1, 28));
  }

  public function run()
  {
    // delete les entrées précédentes de la table chutes
    DB::table('carts')->delete();
    // créer 999 entrées
    for ($i=1 ; $i <=10  ; $i++ ) {
      // générer une date aléatoire
      $date = $this->randDate();
      DB::table('carts')->insert([
        'user_id' => rand(1, 10),
        'chute_id' => rand(1, 999),
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
  }
}

?>
