<?php

namespace App\Repositories;

use App\Models\Designation;

class DesignationrRepository extends ResourceRepository
{

  public function __construct(Designation $designation)
  {
    $this->model = $designation;
    $this->designation = $designation;
  }

}
