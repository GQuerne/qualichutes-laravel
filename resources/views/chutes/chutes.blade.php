@extends('template')

@section('title')
	Chutes
@endsection

@section('content')
	@if(Auth::check() and Auth::user()->admin)
		{!! link_to_route('chute.create', 'Ajouter une chute', [], ['class' => 'btn bg-lightGreen float-right']) !!}
	@endif

	@if (isset($info))
		<div class="row alert alert-info"> {{ $info }} </div>
	@endif

	<div class="mt-3 ml-5">
		{!! $links !!}
	</div>

	{{-- $table->increments('id');
	$table->timestamps();
	$table->integer('nuance_id')->unsigned(); //foreignKey
	$table->foreign('nuance_id')->references('id')->on('nuances')->onDelete('restrict')->onUpdate('restrict');
	$table->string('CCPU')->nullable();
	$table->enum('format', array('indifférent', 'rond', 'carre', '6Pans', 'tubes'))->default('indifférent');
	$table->string('diam')->nullable();
	$table->string('epaisseur')->nullable();
	$table->string('cote')->nullable();
	$table->string('longueurCoupe')->nullable();
	$table->string('poidsTheorique')->nullable(); // fill by controller dimensions*nuance->densite
	$table->float('poidsReel')->nullable();
	$table->float('prixHorsTaxe')->nullable(); --}}

	<div class="row">
		@foreach ($chutes as $chute)
			<div class="card col-sm-3">
				<div class="card-header bg-lightGrey">
					<a href="chute/{{$chute->id}}"> {{ $chute->id }}</a>
					<span class="float-right"> {{$chute->nuance->nuanceName}} </span>
				</div>
				<div class="card-body">
					<table>
						<tr>
							<td>Format: </td>
							<td class="col-sm-4"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></td>
							<td> {{ $chute->format }} </td>
						</tr>
						@if (isset($chute->diam))
							<tr>
								<td>Diamètre: </td>
								<td class="col-sm-4"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></td>
								<td> {{ $chute->diam }} </td>
							</tr>
						@endif
						@if (isset($chute->epaisseur))
							<tr>
								<td>Épaisseur: </td>
								<td class="col-sm-4"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></td>
								<td> {{ $chute->epaisseur }} </td>
							</tr>
						@endif
						@if (isset($chute->cote))
							<tr>
								<td>Côté: </td>
								<td class="col-sm-4"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></td>
								<td> {{ $chute->cote }} </td>
							</tr>
						@endif
						@if (isset($chute->longueurCoupe))
							<tr>
								<td>Longueur de coupe: </td>
								<td class="col-sm-4"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></td>
								<td> {{ $chute->longueurCoupe }} </td>
							</tr>
						@endif
						<tr>
							<td>Poids: </td>
							<td class="col-sm-4"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></td>
							<td> {{ $chute->poidsTheorique }} </td>
						</tr>
					</table>
				</div>
			</div>
		@endforeach
	</div>
	{!! $links !!}
@endsection
