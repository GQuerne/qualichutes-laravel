@extends('template')

@section('title')
  Search results
@endsection

@section('content')
  <div class="container">
    @if (isset($details))
      <p> The search results for your query {{ $query}} are : </p>
      <h2>Nuance search</h2>
      <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Normes <span class="fa fa-arrow-circle-right" aria-hidden="true"></span> Designations</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($details as $nuance)
            <tr>
              <td>{{$nuance->name}}</td>
              <td>
                @foreach ($nuance->designations as $designation)
                  {{$designation->norme}} <span class="fa fa-arrow-circle-right" aria-hidden="true"></span> {{$designation->name}} <br>
                @endforeach
              </td>


              {{-- <td>{{$nuance->designations->name}}</td> --}}
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
  </div>
@endsection
