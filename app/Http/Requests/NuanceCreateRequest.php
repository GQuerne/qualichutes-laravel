<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NuanceCreateRequest extends FormRequest
{

  public function authorize()
  {
    return true;
  }
  // validator
  public function rules()
  {
    return [
      'famille' => 'required',
      'nuanceName' => 'required',
      'densite' => 'required',
    ];
  }
}
