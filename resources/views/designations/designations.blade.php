@extends('template')

@section('title')
	Designations
@endsection

@section('content')
	@if(Auth::check() and Auth::user()->admin)
		{!! link_to_route('designation.create', 'Ajouter une designation', [], ['class' => 'btn btn-info float-right']) !!}
	@endauth
	@if(isset($info))
		<div class="row alert alert-info"> {{ $info }} </div>
	@endif
	<div class="mt-3 ml-5">
		{!! $links !!}
	</div>


	@foreach ($designations as $designation)
		<table class="table">
			<tbody>
				<tr>
					<td>{{ $designation->norme }}</td>
					@if(Auth::check() and Auth::user()->admin)
						<td>
							{!! Form::open(['method' => 'DELETE', 'route' => ['designation.destroy', $designation->id]]) !!}
							{!! Form::submit('Supprimer cette designation', ['class' => 'btn btn-danger', 'onclick' => 'return confirm(\'Voulez-vous vraiment supprimer cette designation ?\')']) !!}
							{!! Form::close() !!}
						</td>
					@endif
				</tr>
				<tr>
					<td>{{ $designation->name }}</td>
				</tr>
			</tbody>
		</table>
	@endforeach
	{!! $links !!}
@endsection
