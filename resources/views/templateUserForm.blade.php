@extends('template')

@section('content')
  <div class="container">
    <div class="card col-md-10">
      @yield('card-header')
      {{-- <div class="card-header">{{ __('Register') }}
    </div> --}}
    {{-- form --}}
    <div class="card-body">
      @yield('form-open')
      {{-- <form method="POST" action="{{ route('register') }}"> --}}
      @csrf
      {{-- email --}}
      <div class="form-group row">
        <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
        <div class="col-md-9">
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
        </div>
        {{-- email_confirmation --}}
        <div class="form-group row">
          <label for="email-confirm" class="col-md-3 col-form-label text-md-right">{{ __('Confirm Email') }}</label>
          <div class="col-md-9">
            <input id="email-confirm" type="email" class="form-control" name="email_confirmation" required autocomplete="email">
          </div>
        </div>
        {{-- password --}}
        <div class="form-group row">
          <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>
          <div class="col-md-9">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
              @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
          </div>
          {{-- password_confirmation --}}
          <div class="form-group row">
            <label for="password-confirm" class="col-md-3 col-form-label text-md-right">{{ __('Confirm password') }}</label>
            <div class="col-md-9">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
          </div>
          {{-- gender need checked to have a default value or get undefined index at create --}}
          <div class="form-group row">
            <label for="gender" class="col-md-3 col-form-label text-md-right">{{ __('Gender') }}</label>
            <div class="col-md-9">
              <input type="radio" id="Mr" name="gender" value="Mr"> <label for="Mr"> Mr </label>
              <input type="radio" id="Mme" name="gender" value="Mme"> <label for="Mme"> Mme </label>
              <input type="radio" id="NC" name="gender" value="NC" checked> <label for="NC"> NC </label>
              @error('gender')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
          </div>
          {{-- lastname --}}
          <div class="form-group row">
            <label for="lastName" class="col-md-3 col-form-label text-md-right">{{ __('Last name') }}</label>
            <div class="col-md-9">
              <input id="lastName" type="text" class="form-control @error('lastName') is-invalid @enderror" name="lastName" value="{{ old('lastName') }}" autocomplete="lastName">
                @error('lastName')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            {{-- firstname --}}
            <div class="form-group row">
              <label for="firstName" class="col-md-3 col-form-label text-md-right">{{ __('First name') }}</label>
              <div class="col-md-9">
                <input id="firstName" type="text" class="form-control @error('firstName') is-invalid @enderror" name="firstName" value="{{ old('firstName') }}" autocomplete="firstName">
                  @error('firstName')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              {{-- company --}}
              <div class="form-group row">
                <label for="company" class="col-md-3 col-form-label text-md-right">{{ __('Company name') }}</label>
                <div class="col-md-9">
                  <input id="company" type="text" class="form-control @error('company') is-invalid @enderror" name="company" value="{{ old('company') }}" autocomplete="company">
                    @error('company')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                  </div>
                </div>
                {{-- siret --}}
                <div class="form-group row">
                  <label for="siret" class="col-md-3 col-form-label text-md-right">{{ __('SIRET') }}</label>
                  <div class="col-md-9">
                    <input id="siret" type="text" class="form-control @error('siret') is-invalid @enderror" name="siret" value="{{ old('siret') }}" autocomplete="siret">
                      @error('siret')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                  </div>
                  {{-- address --}}
                  {{-- addressLine1 input --}}
                  <div class="form-group row">
                    <label for='addressLine1' class="col-md-3 col-form-label text-md-right">{{ __('Address Line 1') }}</label>
                    <div class="col-md-9">
                      <input id="addressLine1" type="text" class="form-control @error ('addressLine1') is-invalid @enderror" name="addressLine1"  value="{{ old('addressLine1') }}" autocomplete="addressLine1">
                        <p class="help-block">Street address, P.O. box, company name, c/o</p>
                      </div>
                    </div>
                    {{-- addressLine2 input--}}
                    <div class="form-group row">
                      <label for='addressLine2' class="col-md-3 col-form-label text-md-right">{{ __('Address Line 2') }}</label>
                      <div class="col-md-9">
                        <input id="addressLine2" type="text" class="form-control @error ('addressLine2') is-invalid @enderror" name="addressLine2" value="{{ old('addressLine2') }}" autocomplete="addressLine2">
                          <p class="help-block">Apartment, suite , unit, building, floor, etc.</p>
                        </div>
                      </div>
                      <!-- postal-code input-->
                      <div class="form-group row">
                        <label for='postCode' class="col-md-3 col-form-label text-md-right">{{__('Zip or Postal code') }}</label>
                        <div class="col-md-9">
                          <input id="postCode" type="text" class="form-control @error ('postCode') is-invalid @enderror" name="postCode" value="{{ old('postCode') }}" autocomplete="postCode">
                          </div>
                        </div>
                        <!-- city input-->
                        <div class="form-group row">
                          <label for='city' class="col-md-3 col-form-label text-md-right">{{ __('City or town') }}</label>
                          <div class="col-md-9">
                            <input id="city" type="text" class="form-control @error ('city') is-invalid @enderror" name="city"  value="{{ old('city') }}" autocomplete="city">
                            </div>
                          </div>

                          <!-- country select -->
                          <div class="form-group row">
                            <label for='country' class="col-md-3 col-form-label text-md-right">{{__('Country') }}</label>
                            <div class="controls">
                              <select id="country" class="form-control @error ('country') is-invalid @enderror" name="country">
                                <option value="" selected="selected">(please select a country)</option>
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarctica</option>
                                <option value="AG">Antigua and Barbuda</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="AU">Australia</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BB">Barbados</option>
                                <option value="BY">Belarus</option>
                                <option value="BE">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="BT">Bhutan</option>
                                <option value="BO">Bolivia</option>
                                <option value="BA">Bosnia and Herzegowina</option>
                                <option value="BW">Botswana</option>
                                <option value="BV">Bouvet Island</option>
                                <option value="BR">Brazil</option>
                                <option value="IO">British Indian Ocean Territory</option>
                                <option value="BN">Brunei Darussalam</option>
                                <option value="BG">Bulgaria</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BI">Burundi</option>
                                <option value="KH">Cambodia</option>
                                <option value="CM">Cameroon</option>
                                <option value="CA">Canada</option>
                                <option value="CV">Cape Verde</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="CF">Central African Republic</option>
                                <option value="TD">Chad</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CX">Christmas Island</option>
                                <option value="CC">Cocos (Keeling) Islands</option>
                                <option value="CO">Colombia</option>
                                <option value="KM">Comoros</option>
                                <option value="CG">Congo</option>
                                <option value="CD">Congo, the Democratic Republic of the</option>
                                <option value="CK">Cook Islands</option>
                                <option value="CR">Costa Rica</option>
                                <option value="CI">Cote d'Ivoire</option>
                                <option value="HR">Croatia (Hrvatska)</option>
                                <option value="CU">Cuba</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czech Republic</option>
                                <option value="DK">Denmark</option>
                                <option value="DJ">Djibouti</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="TP">East Timor</option>
                                <option value="EC">Ecuador</option>
                                <option value="EG">Egypt</option>
                                <option value="SV">El Salvador</option>
                                <option value="GQ">Equatorial Guinea</option>
                                <option value="ER">Eritrea</option>
                                <option value="EE">Estonia</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FK">Falkland Islands (Malvinas)</option>
                                <option value="FO">Faroe Islands</option>
                                <option value="FJ">Fiji</option>
                                <option value="FI">Finland</option>
                                <option value="FR">France</option>
                                <option value="FX">France, Metropolitan</option>
                                <option value="GF">French Guiana</option>
                                <option value="PF">French Polynesia</option>
                                <option value="TF">French Southern Territories</option>
                                <option value="GA">Gabon</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgia</option>
                                <option value="DE">Germany</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="GL">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="GT">Guatemala</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-Bissau</option>
                                <option value="GY">Guyana</option>
                                <option value="HT">Haiti</option>
                                <option value="HM">Heard and Mc Donald Islands</option>
                                <option value="VA">Holy See (Vatican City State)</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hong Kong</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IR">Iran (Islamic Republic of)</option>
                                <option value="IQ">Iraq</option>
                                <option value="IE">Ireland</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="JO">Jordan</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="KE">Kenya</option>
                                <option value="KI">Kiribati</option>
                                <option value="KP">Korea, Democratic People's Republic of</option>
                                <option value="KR">Korea, Republic of</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LA">Lao People's Democratic Republic</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LS">Lesotho</option>
                                <option value="LR">Liberia</option>
                                <option value="LY">Libyan Arab Jamahiriya</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MO">Macau</option>
                                <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                <option value="MG">Madagascar</option>
                                <option value="MW">Malawi</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Maldives</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MQ">Martinique</option>
                                <option value="MR">Mauritania</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="MX">Mexico</option>
                                <option value="FM">Micronesia, Federated States of</option>
                                <option value="MD">Moldova, Republic of</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolia</option>
                                <option value="MS">Montserrat</option>
                                <option value="MA">Morocco</option>
                                <option value="MZ">Mozambique</option>
                                <option value="MM">Myanmar</option>
                                <option value="NA">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="NP">Nepal</option>
                                <option value="NL">Netherlands</option>
                                <option value="AN">Netherlands Antilles</option>
                                <option value="NC">New Caledonia</option>
                                <option value="NZ">New Zealand</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NU">Niue</option>
                                <option value="NF">Norfolk Island</option>
                                <option value="MP">Northern Mariana Islands</option>
                                <option value="NO">Norway</option>
                                <option value="OM">Oman</option>
                                <option value="PK">Pakistan</option>
                                <option value="PW">Palau</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippines</option>
                                <option value="PN">Pitcairn</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="QA">Qatar</option>
                                <option value="RE">Reunion</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russian Federation</option>
                                <option value="RW">Rwanda</option>
                                <option value="KN">Saint Kitts and Nevis</option>
                                <option value="LC">Saint LUCIA</option>
                                <option value="VC">Saint Vincent and the Grenadines</option>
                                <option value="WS">Samoa</option>
                                <option value="SM">San Marino</option>
                                <option value="ST">Sao Tome and Principe</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="SN">Senegal</option>
                                <option value="SC">Seychelles</option>
                                <option value="SL">Sierra Leone</option>
                                <option value="SG">Singapore</option>
                                <option value="SK">Slovakia (Slovak Republic)</option>
                                <option value="SI">Slovenia</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="SO">Somalia</option>
                                <option value="ZA">South Africa</option>
                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                <option value="ES">Spain</option>
                                <option value="LK">Sri Lanka</option>
                                <option value="SH">St. Helena</option>
                                <option value="PM">St. Pierre and Miquelon</option>
                                <option value="SD">Sudan</option>
                                <option value="SR">Suriname</option>
                                <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                <option value="SZ">Swaziland</option>
                                <option value="SE">Sweden</option>
                                <option value="CH">Switzerland</option>
                                <option value="SY">Syrian Arab Republic</option>
                                <option value="TW">Taiwan, Province of China</option>
                                <option value="TJ">Tajikistan</option>
                                <option value="TZ">Tanzania, United Republic of</option>
                                <option value="TH">Thailand</option>
                                <option value="TG">Togo</option>
                                <option value="TK">Tokelau</option>
                                <option value="TO">Tonga</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="TN">Tunisia</option>
                                <option value="TR">Turkey</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TC">Turks and Caicos Islands</option>
                                <option value="TV">Tuvalu</option>
                                <option value="UG">Uganda</option>
                                <option value="UA">Ukraine</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="GB">United Kingdom</option>
                                <option value="US">United States</option>
                                <option value="UM">United States Minor Outlying Islands</option>
                                <option value="UY">Uruguay</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VE">Venezuela</option>
                                <option value="VN">Viet Nam</option>
                                <option value="VG">Virgin Islands (British)</option>
                                <option value="VI">Virgin Islands (U.S.)</option>
                                <option value="WF">Wallis and Futuna Islands</option>
                                <option value="EH">Western Sahara</option>
                                <option value="YE">Yemen</option>
                                <option value="YU">Yugoslavia</option>
                                <option value="ZM">Zambia</option>
                                <option value="ZW">Zimbabwe</option>
                              </select>
                            </div>
                          </div>
                          {{-- tel --}}
                          <div class="form-group row">
                            <label for="tel" class="col-md-3 col-form-label text-md-right">{{ __('Phone') }}</label>
                            <div class="col-md-9">
                              <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ old('tel') }}" autocomplete="tel">
                                @error('tel')
                                  <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                @enderror
                              </div>
                            </div>
                            {{-- fax --}}
                            <div class="form-group row">
                              <label for="fax" class="col-md-3 col-form-label text-md-right">{{ __('Fax') }}</label>
                              <div class="col-md-9">
                                <input id="fax" type="text" class="form-control @error('fax') is-invalid @enderror" name="fax" value="{{ old('fax') }}" autocomplete="fax">
                                  @error('fax')
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                    </span>
                                  @enderror
                                </div>
                              </div>
                              {{-- pay  need checked to have a default value or get undefined index at create  --}}
                              <div class="form-group row">
                                <label for="pay" class="col-md-3 col-form-label text-md-right">{{ __('pay') }}</label>
                                <div class="col-md-9">
                                  <input type="radio" id="LCR" name="pay" value="LCR"> <label for="LCR"> LCR </label>
                                  <input type="radio" id="virement" name="pay" value="virement"> <label for="virement"> virement </label>
                                  <input type="radio" id="paypal" name="pay" value="paypal" checked> <label for="paypal"> paypal </label>
                                  <input type="radio" id="chèque" name="pay" value="chèque"> <label for="chèque"> chèque </label>
                                  @error('pay')
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                    </span>
                                  @enderror
                                </div>
                              </div>
                              {{-- fidélité  need checked to have a default value or get undefined index at create --}}
                              <div class="form-group row">
                                <label for="fidel" class="col-md-3 col-form-label text-md-right">{{ __('fidel') }}</label>
                                <div class="col-md-9">
                                  <input type="radio" id="newcomer" name="fidel" value="newcomer" checked> <label for="newcomer"> Newcomer </label>
                                  <input type="radio" id="gold" name="fidel" value="gold"> <label for="gold"> Gold </label>
                                  <input type="radio" id="platinium" name="fidel" value="platinium"> <label for="platinium"> Platinium </label>
                                  @error('fidel')
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                    </span>
                                  @enderror
                                </div>
                              </div>
                              @yield('submit')
                              <a href="javascript:history.back()" class="btn bg-lightGrey float-left"> <span class="fa fa-arrow-circle-left"> </span> Retour </a>
                              {{-- <button type="submit" class="btn btn-primary"> {{ __('Register') }} </button> --}}
                            </form>
                          </div>
                        </div>
                      </div>
                    @endsection
