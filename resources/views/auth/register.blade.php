@extends('templateUserForm')

@section('title')
  Register
@endsection

@section('card-header')
  <div class="card-header {!! link_to('register', 'S\'inscrire', ['class' => 'btn btn-success float-right']) !!}">{{ __('Register') }}
  </div>
@endsection

@section('form-open')
  <form method="POST" action="{{ route('register') }}">
@endsection

@section('submit')
  <button type="submit" class="btn bg-lightGreen float-right"> {{ __('Register') }} </button>
@endsection
