@extends('template')

@section('content')
	<div class="container">
		<div class="row jutify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						Éditer la nuance {{ $nuance->nuanceName }}
					</div>
					<div class="card-body">
						{!! Form::model($nuance, ['route' => ['nuance.update', $nuance->id] , 'method' => 'PUT']) !!}
						{{-- famille --}}
						<div class="form-group row">
							{!! Form::label('famille', 'famille', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
							<div class="col-md-6">
								<input type="radio" name="famille"  value="acConstrAllie"	{{ $nuance->famille == 'acConstrAllie' ? 'checked' : '' }} >	Aciers de Construction et Alliés <br>
								<input type="radio" name="famille"  value="acInox"				{{ $nuance->famille == 'acInox' ? 'checked' : '' }} >					Aciers Inoxydables <br>
								<input type="radio" name="famille"  value="acOutils"			{{ $nuance->famille == 'acOutils' ? 'checked' : '' }} >				Aciers Outils <br>
								<input type="radio" name="famille"  value="plastTech"			{{ $nuance->famille == 'plastTech' ? 'checked' : '' }} >			Plastiques Techniques <br>
								<input type="radio" name="famille"  value="alu"						{{ $nuance->famille == 'alu' ? 'checked' : '' }} >						Aluminiums <br>
								<input type="radio" name="famille"  value="cuivre"				{{ $nuance->famille == 'cuivre' ? 'checked' : '' }} >					Cuivreux <br>
								<input type="radio" name="famille"  value="bronze"				{{ $nuance->famille == 'bronze' ? 'checked' : '' }} >					Bronzes <br>
								<input type="radio" name="famille"  value="acEtire"				{{ $nuance->famille == 'acEtire' ? 'checked' : '' }} >				Aciers Étirés <br>
								<input type="radio" name="famille"  value="tubeMeca"			{{ $nuance->famille == 'tubeMeca' ? 'checked' : '' }} >				Tubes Mécaniques <br>
								<input type="radio" name="famille"  value="tigeChrome"		{{ $nuance->famille == 'tigeChrome' ? 'checked' : '' }} >			Tiges Chromées <br>
								<input type="radio" name="famille"  value="fonte"					{{ $nuance->famille == 'fonte' ? 'checked' : '' }} >					Fontes <br>
								<input type="radio" name="famille"  value="acRessort"			{{ $nuance->famille == 'acRessort' ? 'checked' : '' }} >			Aciers à Ressort <br>
								<br>
							</div>
						</div>
						{{-- name --}}
						<div class="form-group row">
							{!! Form::label('nuanceName', 'Name:', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
							<div class="col-md6-">
								{!! Form::text('nuanceName', null, ['placeholder' => 'nuanceName']) !!}
							</div>
						</div>
						{{-- designations --}}
						<div class="form-group row">
							{!! Form::label('designations', 'Désignations:', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
							@foreach ($nuance->designations as $designation)
								<div class="col-md6-">
									{!! Form::text('designationNorme', $designation->designationNorme) !!}
									{!! Form::text('designationName', $designation->designationName) !!}
								</div>
							@endforeach
						</div>
						{{-- densite --}}
						<div class="form-group row">
							{!! Form::label('densite', 'Densite:', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
							<div class="col-md-6">
								{!! Form::text('densite', null, ['placeholder' =>'densite']) !!}
							</div>
						</div>
						{!! Form::submit('Éditer', ['class' => 'btn bg-green float-right']) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="javascript:history.back()" class="btn bg-lightGrey"> <span class="fa fa-arrow-circle-left"> </span> Retour </a>
@endsection
