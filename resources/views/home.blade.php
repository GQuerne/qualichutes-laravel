@extends('template')

@section('title')
    QualiChutes
@endsection

@section('content')
    <h1>Accueil</h1>

    <h2> install the project</h2>
    <ol>
      <li>install laragon</li>
      <li>clone from git in laragon www</li>
      <li>install composer dependencies <i>composer install</i> </li>
      <li>install npm dependencies <i>npm install</i> </li>
      <li>copy .env <i>cp .env.example .env</i> </li>
      <li>generate app encryption key <i>php artisan key:generate</i> </li>
      <li>create empty database</li>
      <li>config .env to connect to the empty database</li>
      <li>migrate the database <i>php artisan migrate</i> </li>
      <li>seed <i>php artisan db:seed</i> </li>
    </ol>
@endsection
