<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NuanceUpdateRequest extends FormRequest
{

  public function authorize()
  {
    return true;
  }
  // validator
  public function rules()
  {
    // $id = $this->nuance;
    return [
      'famille' => 'required',
      'nuanceName' => 'required|unique:nuances',
      'densite' => 'required',
    ];
  }
}
