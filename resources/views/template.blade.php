<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>@yield('title')</title>
  {{-- liens externes --}}
  <!-- Scripts -->

  <!-- Styles -->
  {{-- icons --}}


  {{-- liens internes bootstrap4 script--}}
  <script src="{{ URL::asset('js/bootstrap4.js') }}" defer></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  {{-- liens internes bootstrap4 css and fontawesome icons--}}
  <link rel = "stylesheet" href = "{{ URL::asset('css/bootstrap4.css') }}"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style> textarea { resize: none; } </style>
</head>

<body>
  <header class="jumbotron">
    <div class="container-fluid">
      <h1 class="page-header"> QualiChutes</h1>
      <form class="float-left" action="/search" method="post" role="search">
        {{ csrf_field() }}
        <div class="input-group">
          <input type="text" class="form-control" name="q" placeholder="Search nuance">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
              <span class="fa fa-search" aria-hidden="true">
              </span>
            </button>
          </span>
        </div>
      </form>

      @guest
        {!! link_to('register', 'S\'inscrire', ['class' => 'btn bg-green float-right']) !!}
        {!! link_to('login', 'Se connecter', ['class' => 'btn bg-lightBlue float-right']) !!}
      @endguest
      @auth
        <a class="btn bg-yellow float-right" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> {{ __('Déconnexion') }} </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
        {!! link_to('user/' . Auth::user()->id, Auth::user()->firstName, ['class' => 'btn bg-lightBlue float-right']) !!}
      @endauth
    </div>
  </header>

  <nav class="navbar navbar-expand-md navbar-light bg-lightBlue">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('home')}}"> Home </a>
      </li>
      @if(Auth::check() and Auth::user()->admin)
        <li class="nav-item">
          <a class="nav-link" href="{{ route('user.index')}}"> Users </a>
        </li>
      @endif
      <li class="nav-item">
        <a class="nav-link" href="{{ route('nuance.index')}}"> Nuances </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Nuances List </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          @foreach ($allNuances as $item)
            <a class="dropdown-item" href="{{ route('nuance.show', $item->id) }}"> {{$item->nuanceName}}</a>
          @endforeach
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('chute.index')}}"> Chutes </a>
      </li>
      {{-- <li class="nav-item"> <a class="nav-link" href="{{ route('designation.index')}}"> Designations  </a> </li> --}}
    </ul>
  </nav>

  @yield('subNavBar')

  <div class="container-fluid">
    @yield('content')
  </div>

</body>
</html>
