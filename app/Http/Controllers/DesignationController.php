<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\DesignationRepository;

use App\Http\Requests\DesignationRequest;

class DesignationController extends Controller
{
  protected $designationRepository;

  // nouvelle instance du controleur
  public function __construct(DesignationRepository $designationRepository)
  {
    // les utilisateurs non identifié ne peuvent voir que la fonction index
    $this->middleware('auth', ['except' => 'index']);
    $this->designationRepository = $designationRepository;
  }

  public function index()
  {
    // afficher 8 désignations par page
    $designations = $this->designationRepository->getPaginate(8);
    $links = $designations->render();
    return view('designations.designations', compact('designations', 'links'));
  }

  // créer une nouvelle désignation
  public function create()
  {
    // formulaire de création d'une désignation
    return view('designations.designationsAdd');
  }

  // stocker la désignation en base
  public function store(DesignationRequest $request)
  {
    $designation = $this->designationRepository->store($request->all());
    return redirect('designation');
  }

  public function show($id)
  {

  }

  public function edit($id)
  {

  }

  public function update($id)
  {

  }

  // supprimer une désignation
  public function destroy($id)
  {
    $this->designationRepository->destroy($id);
    return back();
  }

}

?>
