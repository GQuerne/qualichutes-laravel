{!! Form::open(array('route' => 'designation.store', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('nuance_id', 'Nuance_id:') !!}
			{!! Form::text('nuance_id') !!}
		</li>
		<li>
			{!! Form::label('norme', 'Norme:') !!}
			{!! Form::text('norme') !!}
		</li>
		<li>
			{!! Form::label('name', 'Name:') !!}
			{!! Form::text('name') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}
