<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends ResourceRepository
{

  public function __construct(User $user)
  {
    $this->model = $user;
    $this->user = $user;
  }

  public function store($inputs)
  {
    $this->user->create($inputs);
  }

}
