<?php

namespace App\Repositories;

use App\Models\Nuance;

class NuanceRepository
{
  protected $nuance;

  public function __construct(Nuance $nuance)
  {
    $this->model = $nuance;
    $this->nuance = $nuance;
  }

  // findById
  public function getById($id)
  {
    return $this->nuance->findOrFail($id);
  }

  // paginate
  public function getPaginate()
  {
    return $this->nuance->groupBy('nuances.famille')->paginate(count());
  }

  // send in DB
  public function store($inputs)
  {
    $this->nuance->create($inputs);
  }

  public function update($id, Array $inputs)
  {
    $this->getById($id)->update($inputs);
  }

  // delete
  public function destroy($id)
  {
    $this->nuance->findOrFail($id)->delete();
  }

  // findbyFamily
  public function getByFamily($famille)
  {
    return $this->nuance->findOrFail($famille);
  }

  // include designations in nuances results
  private function queryWithDesignation()
  {
    return $this->nuance->with('designations')->orderBy('nuances.famille');
  }

  // find nuance by Id including designations
  public function getByIdWithDesignation($id)
  {
    return $this->nuance->with('designations')->findOrFail($id);
  }

  // get with designations (don't need to look after in Controller)
  public function getWithDesignationAndPaginate($n)
  {
    return $this->queryWithDesignation()->paginate($n);
  }

  // get by Designations
  public function getWithDesignationForSearch($designation, $n)
  {
    return $this->queryWithDesignation()->whereHas('designations', function($q) use ($designation)
    {
      $q->where('designations.name', $designation);
    })->orWhere('nuances.name', 'LIKE', '%'.$designation.'%')->paginate($n);
  }

}
