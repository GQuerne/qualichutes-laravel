<?php

namespace App\Providers;

use App\Models\Nuance;
use View;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(255);
        View::composer('*', function($view)
        {
          $allNuances = Nuance::all();
          $view->with('allNuances', $allNuances);
        });
    }
}
