<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('email')->unique();
      $table->string('password');
      $table->enum('gender', ['Mr', 'Mme', 'NC'])->nullable();
      $table->string('lastName')->nullable();
      $table->string('firstName')->nullable();
      // $table->timestamp('email_verified_at')->nullable();
      $table->string('company')->nullable();
      $table->string('siret')->unique()->nullable();
      $table->string('addressLine1');
      $table->string('addressLine2')->nullable();
      $table->string('postCode');
      $table->string('city');
      $table->string('country');
      $table->string('tel');
      $table->string('fax')->nullable();
      $table->enum('pay', ['LCR', 'virement', 'paypal', 'chèque'])->nullable();
      $table->enum('fidel', ['newcomer', 'gold', 'platinium'])->nullable();
      $table->boolean('admin')->default(false);
      $table->rememberToken();
      $table->timestamps();
    });
  }

  public function down()
  {
    Schema::dropIfExists('users');
  }
}
