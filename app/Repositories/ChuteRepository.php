<?php

namespace App\Repositories;

use App\Models\Chute;

class ChuteRepository extends ResourceRepository
{

  public function __construct(Chute $chute)
  {
    $this->model = $chute;
    $this->chute = $chute;
  }

  public function store($inputs)
  {
    $this->chute->create($inputs);
  }

}
