<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

// $table->increments('id');
// $table->timestamps();
// $table->integer('nuance_id')->unsigned(); //foreignKey
// $table->foreign('nuance_id')->references('id')->on('nuances')->onDelete('restrict')->onUpdate('restrict');
// $table->string('CCPU')->nullable();
// $table->enum('format', array('indifférent', 'rond', 'carre', '6Pans', 'tubes'))->default('indifférent');
// $table->string('diam')->nullable();
// $table->string('epaisseur')->nullable();
// $table->string('cote')->nullable();
// $table->string('longueurCoupe')->nullable();
// $table->string('poidsTheorique')->nullable(); // fill by controller dimensions*nuance->densite
// $table->float('poidsReel')->nullable();
// $table->float('prixHorsTaxe')->nullable();

class ChutesTableSeeder extends Seeder
{
  private function randDate()
  {
    return Carbon::createFromDate(null, rand(1, 12), rand(1, 28));
  }

  public function run()
  {
    // delete les entrées précédentes de la table chutes
    DB::table('chutes')->delete();
    // créer 999 entrées
    for ($i=1 ; $i <=999  ; $i++ ) {
      // générer une date aléatoire
      $date = $this->randDate();
      DB::table('chutes')->insert([
        'nuance_id' => rand(1, 124),
        'ccpu' => 'ccpu' . $i,
        'format' => 'indifférent',
        'diam' => rand(1, 9999),
        'epaisseur' => rand(1, 9999),
        'cote' => rand(1, 9999),
        'longueurCoupe' => rand(1, 9999),
        'poidsTheorique' => rand(1, 9999),
        'poidsReel' => rand(1, 9999),
        'prixHorsTaxe' => rand(1, 9999),
        'created_at' => $date,
        'updated_at' => $date
      ]);
    }
  }
}

?>
